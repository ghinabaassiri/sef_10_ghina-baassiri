function newElement(){
	var task = document.todoList.task.value;
	var description = document.todoList.desc.value;
	var list = document.getElementById("list").innerHTML;
	
	var now = new Date();
	var mm = now.getMonth()+1; //January is 0!
	var yyyy = now.getFullYear();
	var hr = now.getHours();
	var min = now.getMinutes();
	var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
	var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

	var day = days[ now.getDay() ];
	var month = months[ now.getMonth() ];
	
	var today = day+', '+month+' '+mm+' '+yyyy+' '+hr+':'+min;
	if(task=="" ||description==""){
		alert("Please enter a task first.");
		return 0;
	}
	var arr = document.getElementsByClassName("item");
	var listId ="divnb"+arr.length;
	document.getElementById("list").innerHTML =
	
	"<li class='item' id='"+listId+"'>\
			<button type='button' onclick='deleteElement(parentNode.id)'>x</button>\
			<div class='separator2'> </div>\
			<div>\
				<h3>"+task+"</h3>\
				<h5>Added:"+today+"</h5>\
				<h4>"+description+"</h4>\
			</div>\
		</li>"+list;
	
	document.todoList.task.value="";
	document.todoList.desc.value="";	
}

function deleteElement(clicked_id){
	document.getElementById(clicked_id).style.display="none";
	
}