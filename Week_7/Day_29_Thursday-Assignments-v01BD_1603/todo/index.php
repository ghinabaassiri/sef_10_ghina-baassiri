<!DOCTYPE html>
<html>
<head>
	<title>SEF ToDo List</title>
	<link rel="stylesheet" href="style.css">		
	<script src="script.js"></script> 
</head>
<body>
	<div id="todo">
		<h2>SEF Todo List</h2>
		<h3>Item</h3>
		<form name="todoList">
			<div id="input">
				<input type="text" name="task" placeholder="Enter Task Here" id="entry"/>
				<button type="button" onclick="newElement()" id="add">Add</button>
			</div>
			<div id="description">
				<textarea name="desc" placeholder="Description Here"></textarea>
			</div>
		</form>
		<div class="separator"> </div>
		<ul id="list"></ul>
	</div>

</body>
</html>