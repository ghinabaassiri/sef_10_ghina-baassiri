<?php
require_once('GameGenerator.php');
require_once('GameSolver.php');
class GameOutput extends GameGenerator{
	
	/**
 	 * PrintFinalList
 	 *
 	 * Prints the elements of the randomly generated list
 	 */
	function PrintFinalList(){
		echo "{";
		
		foreach($this->finallist as $key => $value){
			if($key!=count($this->finallist)-1)
			{
				echo $value.",";
			}
			else
			{
				echo $value;
			}
		}
		echo "}\n";
	}
	/**
 	 * PrintFinalNumber
 	 *
 	 * Prints the randomly generated target number 
 	 */
	function PrintFinalNumber(){
		$this->GenerateRandomNumber();
		echo "Target: ". $this->finalnumber."\n";
	}
}

echo "How many games would you like me to play today?";
$numberofgames=readline();

for($i=0;$i<$numberofgames;$i++)
{
    echo "\nGame ". ($i+1)." :\n";
    $go=new GameOutput();
	$go->GenerateRandomList();
	$go->GenerateRandomNumber();
	$go->PrintFinalList();
	$go->PrintFinalNumber();
	echo "------\n";

	$gs=new GameSolver();
	//Set the finallist of the gamesolver object to that of gameoutput
	$gs->setfinal($go->getfinallist());

	$error=$go->getfinalnumber();
    $opcomb=$go->getfinallist();
    $gs->solve($error,$opcomb);
}
?>