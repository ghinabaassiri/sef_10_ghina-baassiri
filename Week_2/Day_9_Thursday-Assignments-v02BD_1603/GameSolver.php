<?php
require_once('GameGenerator.php');
class GameSolver extends GameGenerator{

    private $FinalList = array();

    function setfinal($arr){
        $this->FinalList=$arr;
    }
    function getfinal(){
        return $this->FinalList;
    }
    //////////////////////////////////////////////////////
    //          PERMUTATION WITH REPITITION            //
    ////////////////////////////////////////////////////
    function assoc_apply($assoc, $keys) {
        $array = [];
        for ($i = 0; $i < count($keys); $i++) {
            $array[$i] = $assoc[$keys[$i]];
        }
        return $array;
    }
     
    function permutate_vector($perm_vector, $items_count, $exp, $i = 0) {
        if ($i >= $exp) return false;
        $perm_vector[$i]++;
        if ($perm_vector[$i] >= $items_count) {
            $perm_vector[$i] = 0;
            $perm_vector = $this->permutate_vector(
                $perm_vector,
                $items_count,
                $exp,
                $i+1
            );
        }
        return $perm_vector;
    }
     
    function permutations($array, $exp = -1) {
        $exp = ($exp < 0) ? count($array) : $exp;
        if ($exp < 1) {
            return [[]];
        }
     
        $items_count = count($array);
        $perm_vector = array_fill(0, $exp, 0);
     
        $permutations = [];
        while ($perm_vector) {
            $permutations[] = $this->assoc_apply($array, $perm_vector);
            $perm_vector = $this->permutate_vector($perm_vector, $items_count, $exp);
        }
        return $permutations;
    }
    //////////////////////////////////////////////////////
    //          PERMUTATIONS AND COMBINATIONS          //
    ////////////////////////////////////////////////////


    function power_set($in,$minLength = 1) {

       $count = count($in);
       $members = pow(2,$count);
       $return = array();
       for ($i = 0; $i < $members; $i++) {
          $b = sprintf("%0".$count."b",$i);
          $out = array();
          for ($j = 0; $j < $count; $j++) {
             if ($b{$j} == '1') $out[] = $in[$j];
          }
          if (count($out) >= $minLength) {
             $return[] = $out;
          }
       }

       //usort($return,"cmp");  //can sort here by length
       return $return;
    }

    function power_perms($arr) {
        $power_sett = $this->power_set($arr);
        $result = array();
        foreach($power_sett as $set) {
            $perms = $this->perms($set);
            $result = array_merge($result,$perms);
        }
        return $result;
    }

    function factorial($int){
       if($int < 2) {
           return 1;
       }

       for($f = 2; $int-1 > 1; $f *= $int--);

       return $f;
    }

    function perm($arr, $nth = null) {
        if ($nth === null) {
            return $this->perms($arr);
        }

        $result = array();
        $length = count($arr);

        while ($length--) {
            $f = $this->factorial($length);
            $p = floor($nth / $f);
            $result[] = $arr[$p];
            $this->array_delete_by_key($arr, $p);
            $nth -= $p * $f;
        }

        $result = array_merge($result,$arr);
        return $result;
    }

    function perms($arr) {
        $p = array();
        for ($i=0; $i < $this->factorial(count($arr)); $i++) {
            $p[] = $this->perm($arr, $i);
        }
        return $p;
    }

    function array_delete_by_key(&$array, $delete_key, $use_old_keys = FALSE) {
        unset($array[$delete_key]);
        if(!$use_old_keys) {
            $array = array_values($array);
        }
        return TRUE;
    }
    ///////////////////////////////////////////
    //          SOLVING PART                //
    /////////////////////////////////////////

    /**
     * solve
     *
     * Sets the finalnumber value
     */
    function solve($finalnumber,$finallist){
        $time_start = microtime(true);
        $combinations=$this->power_perms($this->getfinal());
        $error=$finalnumber;
        //to save the absolute value of the error
        $terror=0;
        $opcomb=$finallist;
        $opsign=array(); 
        foreach ($combinations as $combination) {
            $comb=$combination;
            $numlength=count($comb);
            $oplength=$numlength-1;
            $permutations = $this->permutations(['+','-','*','/'], $oplength);
            foreach ($permutations as $permutation) {
                $comb = $combination;
                $perm=$permutation;
                $invalid = 0;
                foreach ($perm as $key => $sign) {
                    $i=$key;
                    if($sign=='*'){
                        $comb[$i+1]=$comb[$i]*$comb[$i+1];
                        unset($comb[$i]);
                        unset($perm[$i]);
                    }
                    if($sign=='/'){
                        if($comb[$i]%$comb[$i+1]==0){
                            $comb[$i+1]=$comb[$i]/$comb[$i+1];                  
                            unset($comb[$i]);
                            unset($perm[$i]);
                        }
                        else{
                            $invalid =1;
                            break;
                        }
                    }
                }
                //array_values reorders values of an array
                $comb = array_values($comb);
                $perm = array_values($perm);

                if($invalid==0){
                    foreach ($perm as $key => $sign) {                    
                        $i=$key;
                        if($sign=='+'){
                            $comb[$i+1]=$comb[$i]+$comb[$i+1];
                            //unset deletes an element of an array
                            unset($comb[$i]);
                            unset($perm[$i]);
                        }
                        if($sign=='-'){
                            if($comb[$i]>=$comb[$i+1]){
                                $comb[$i+1]=$comb[$i]-$comb[$i+1];
                                unset($comb[$i]);
                                unset($perm[$i]);
                            }
                            else{
                                $invalid = 1;
                                break;
                            }
                        }
                    }
                }
                $comb = array_values($comb);
                $perm = array_values($perm);
                //Calculate the error
                if($invalid ==0) {
                    if(!isset($comb[0])){
                        $comb[0]=null;
                    }
                    $terror=abs($finalnumber-$comb[0]);

                    if($terror<abs($error))
                    {
                        $error=$finalnumber-$comb[0];
                        $finalvalue=$comb[0];
                        $opcomb=$combination;
                        $opsign=$permutation;
                    }
                    if($error==0){
                        break;
                    }
                }
            }//end of permutations loop
        } //end of combinations loop
        //calculate the duration of the function
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        //Display the solution
        if($error!=0){
            echo "Solution [Remaining: $error]\n";
            
            for($i=0;$i<count($opsign);$i++){
                echo $opcomb[$i]." ".$opsign[$i];
            }
            echo $opcomb[count($opcomb)-1]." = $finalvalue\n";
            echo "Duration \n".$time;
        }
        else{
            echo "Solution [Exact]\n";
            for($i=0;$i<count($opsign);$i++){
                echo $opcomb[$i]." ".$opsign[$i];
            }
            echo $opcomb[count($opcomb)-1]." = $finalvalue\n";
            echo "Duration \n".$time."\n";
        }

    }
}
?>