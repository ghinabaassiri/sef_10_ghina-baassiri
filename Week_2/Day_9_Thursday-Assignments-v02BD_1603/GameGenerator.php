<?php
class GameGenerator{
	private $List1=array(25,50,75,100);
	private $List2=array(1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10);
	private $TotalNumber=6;
	private $RandomNumber1;
	private $RandomNumber2;
	protected $finallist=array();
	protected $finalnumber;

	/**
 	 * getfinallist
 	 *
 	 * Returns the finallist array value
 	 * @return (array of integers) (finallist) 
 	 */
    function getfinallist(){
        return $this->finallist;
    }
	/**
 	 * getfinalnumber
 	 *
 	 * Returns the finalnumber  value
 	 * @return (integer) (finalnumber) 
 	 */
    function getfinalnumber() {
        return $this->finalnumber;
    }
    /**
 	 * setfinalnumber
 	 *
 	 * Sets the finalnumber value
 	 */
    function setfinalnumber($arr){
        $this->finalnumber=$arr;
    }
    /**
 	 * GenerateRandomList
 	 *
 	 * Fills finallist array with elements randomly extracted from two different lists
 	 */
	function GenerateRandomList(){
		$this->RandomNumber1=rand(1,4);
		shuffle($this->List1);
		$this->RandomNumber2=$this->TotalNumber-$this->RandomNumber1;
		shuffle($this->List2);
		for($i=0;$i<$this->RandomNumber1;$i++){
			array_push($this->finallist,$this->List1[$i]);
		}
		for($j=0;$j<$this->RandomNumber2;$j++){
			array_push($this->finallist,$this->List2[$j]);
		}
	}

	/**
 	 * GenerateRandomNumber
 	 *
 	 * Randomly generates an integer and saves it in finalnumber variable
 	 */
	function GenerateRandomNumber(){
		$this->finalnumber=rand(101,999);
	}
}

?>