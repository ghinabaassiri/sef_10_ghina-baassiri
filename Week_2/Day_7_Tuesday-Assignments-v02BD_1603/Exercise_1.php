<?php
echo "Enter the number of hats:";
$hats=array();
$numofhats=readline();
$k=0;
while($k<$numofhats){
	$line=readline();
	$line=ucwords($line);
	if($line=="White"||$line=="Black")
	{
		array_push($hats,$line);
		$k++;
	}
	else
	{
		echo "Enter White or Black only.\n";
	}
}

$persons=array();
$count=0;


for($i=0;$i<count($hats);$i++)
{
	$numof_black=count_Black($i);

	if($i==0)
	{
		if($numof_black%2==0)
			array_push($persons, "Black");
		else
			array_push($persons, "White");
	}
	else
	{
		if($numof_black%2==0 && $persons[0]=="Black")
		{
			array_push($persons,"White");
		}
		else if ($numof_black%2==0 && $persons[0]=="White")
		{
			array_push($persons,"Black");
		}
		else if($numof_black%2!=0 && $persons[0]=="Black")
		{
			array_push($persons,"Black");
		}
		else if($numof_black%2!=0 && $persons[0]=="White")
		{
			array_push($persons,"White");
		}
	}
}
echo "\n";
$counter1=1;
$counter2=0;

foreach($persons as $key => $person){
	if($person==$hats[$key])
		echo "Guess of person ".($key+1)." is ".$person." --Passed\n";
	else
		echo "Guess of person ".($key+1)." is ".$person." --Failed\n";
	$counter1++;
	$counter2++;
}
echo "\n";

function count_Black($current_index){
	global $hats,$count;
	$count=0;
//Count the Black hats seen by the last person
	for($j=1;$j<count($hats);$j++)
	{
		if($j!=$current_index)
		{
			if($hats[$j]=="Black")
			{
				$count++;
			}		
		}
	}
	return $count;
}

?>