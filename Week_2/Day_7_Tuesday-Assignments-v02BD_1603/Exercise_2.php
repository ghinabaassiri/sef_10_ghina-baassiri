<?php

echo "This is Longest Word from a single keyboard row program, enter 1 to insert a file or 2 to write the words.\n";

$selection=readline();

if($selection=="1"){
	echo "Enter file directory\n";
	$directory=readline();
	$myfile=fopen($directory, "r");
	while(!feof($myfile)){
		$line=fgets($myfile);
		echo $line."\n";
		$words=explode($line);
	}
}
else if($selection=="2"){
	echo "Please enter the list of words seperated by a single space.\n";
	$words_string = readline();
	$words = explode(" ",$words_string);
}


$keyboard_keys=array(array('q','w','e','r','t','y','u','i','o','p'),array('s','d','f','g','h','j','k','l'),array('z','x','c','v','b','n','m'));
$letters=array();
$longest_word="";

foreach ($words as $word) {
	$search_from_row_one=0;
	$letters=str_split($word);
	foreach ($keyboard_keys as $key) {
		$search_from_row=count(array_intersect($letters, $key)) == count($letters);
		if($search_from_row==1 && strlen($word)>strlen($longest_word))
		{
			$longest_word=$word;
		}
	}
}
if($longest_word!="")
{
	echo "The longest word is :".$longest_word."\n";
}
else
{
	echo "There is no word composed from a single row of the keyboard"."\n";	
}
?> 