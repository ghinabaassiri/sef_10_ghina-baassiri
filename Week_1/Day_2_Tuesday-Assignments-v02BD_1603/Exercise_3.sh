sudo apt-get update
sudo apt-get install tmux

#to split the screen horizontally 
Ctrl-b "
#to split the screen vertically
Ctrl-b %
#to navigate between the screens
Ctrl-b o