#!/bin/bash

THRESH=80

MonitorVM(){
memory=$(free -m | grep Mem | awk '{print $3/$2*100 }')
intmemory=$(echo $memory | cut -f1 -d ".")
if [ $intmemory -ge $THRESH ];
then
	echo "ALARM: Virtual Memory Usage is " $newmemory"%"
	return 0
else 
	return 1
fi
}

MonitorUsedDiskSpace(){
diskname=($(df -h | grep '/dev/sd[a-z][0-9]' | awk '{print $1}'))
for value in $(df -h | grep '/dev/sd[a-z][0-9]' | tr -d '%' | awk '{print $5 }'); do
	echo $value
	if [ $value -ge THRESH ];
	then
		echo "ALARM:" ${diskname[counter]} "Usage is " $value"%"
		return 0
	else
		return 1
	fi
	((counter++))
done
}

MonitorVM
vm=$?
MonitorUsedDiskSpace
diskspace=$?

if [ $(($vm)) -eq 1 ] && [ $(($diskspace)) -eq 1 ]; then
	echo "Everythis is OK!"
fi

