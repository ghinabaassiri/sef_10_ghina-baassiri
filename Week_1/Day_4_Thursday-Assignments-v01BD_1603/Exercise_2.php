<?php
//read access log file from directory
$myfile=fopen("/var/log/apache2/access.log","r");

while(!feof($myfile)) {
	$file= fgets($myfile) ;
	//The IP chunk of the access log
	$IP=substr($file,0,9);
	//d is the array of substrings of the log split at / 
	$d=split("\/",$file);
	if ( ! isset($d[0])) {
   		$d[0] = null;
	}
	if ( ! isset($d[1])) {
   		$d[1] = null;
	}
	if ( ! isset($d[2])) {
   		$d[2] = null;
	}
	$day=substr($d[0],15,2);	
	$month=$d[1];
	$year=substr($d[2],0,4);
	$time=substr($d[2], 5,8);
	$Date=$year."-".$month."-".$day;
	//t is the array of substrings of the time string split at :
	$t=split(":",$time);
	if ( ! isset($t[0])) {
   		$t[0] = null;
	}
	if ( ! isset($t[1])) {
   		$t[1] = null;
	}
	if ( ! isset($t[2])) {
   		$t[2] = null;
	}
	$finalTime=$t[0]."-".$t[1]."-".$t[2];
	$dayInwords = date('l', strtotime($Date));
	$monthInwords = date('F', strtotime($Date));
	echo($monthInwords);
	//The date chunk of the access log
	$FinalDate=$dayInwords.", ".$monthInwords." ".$day." ".$year." : ".$finalTime;
	//RT is the array of substrings of the time string split at "
	$RT=split("\"",$file);
	if ( ! isset($RT[1])) {
   		$RT[1] = null;
	}
	if ( ! isset($RT[2])) {
   		$RT[2] = null;
	}
	//The response type of the access log
	$ResponseType="\"".$RT[1]."\"";
	//The response code of the access log
	$ResponseCode=substr($RT[2],1,3);
	//The requested access log
	echo $IP." -- ".$FinalDate." -- ".$ResponseType." -- ".$ResponseCode."\n";
}



?>
