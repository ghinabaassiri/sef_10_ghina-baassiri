<?php
//Reads input from terminal using getopt
$input= getopt("i:"); 
$directory=$input['i'];

function listFolderFiles($dir){
    //Scan all files and folders in dir directory
    $folderfiles = scandir($dir);
    echo "Files within".$dir.":\n";
    foreach($folderfiles as $files){
        if($files != '.' && $files != '..'){
            if(is_file($dir.'/'.$files))
           	 	echo "\n".$files;
            if(is_dir($dir.'/'.$files)) 
            	listFolderFiles($dir.'/'.$files);
        }        
    }
}
listFolderFiles($directory);
?>