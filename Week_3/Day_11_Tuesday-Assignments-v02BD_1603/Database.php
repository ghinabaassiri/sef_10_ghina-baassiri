<?php
class Database
{
	/**
	 * Creates a folder which consists of the database.
	 *
 	 * @param string $databaseName Input as string value of the database name.
 	 */
	public function createDatabase($databaseName)
	{
		if (!file_exists("$databaseName")) {
			mkdir($databaseName);
			echo "\"$databaseName\" CREATED\n";
		} else {
			echo "This database already exists!\n";
		}
	}
	/**
	 * Deletes the database folder.
	 *
 	 * @param string $databaseName Input as string value of the database name.
 	 */
	public function deleteDatabase($databaseName)
	{
		if (file_exists("$databaseName")) {
			$files = array_diff(scandir($databaseName), array('.','..')); 
			foreach ($files as $file) { 
				//if it is a directory we need to scan for the subfolder to remove any files it may contain then remove it and continue
				if (is_dir("$databaseName/$file")) {
	      			deleteDatabase("$databaseName/$file");
				} else {
					unlink("$databaseName/$file"); 
				}
			} 
			echo "\"$databaseName\" DELETED\n";	
			rmdir($databaseName); 
		} else {
			echo "This database doesn't exist!\n";
		}
	}
}
?>