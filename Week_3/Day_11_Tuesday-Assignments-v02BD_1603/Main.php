<?php
/* @author: Ghina Baassiri
 * @description: Week 3 - Database Program 
 * based solution for exercise 1
 */

require_once('Database.php');
require_once('Table.php');

echo "Hello ". get_current_user().",\n"."This is a simple database program.\nHere are the queries that you can use:\n";
echo "	CREATE,DATABASE,\"Database Name\"\n"; 
echo "	DELETE,DATABASE,\"Database Name\"\n"; 
echo "	CREATE,TABLE,\"Table Name\",COLUMNS,\"ID\",\"Column1\",\"Column2\"..\n";
echo "	DELETE,TABLE,\"Table Name\"\n";
echo "	ADD,\"ID\",\"Column1 Value\",\"Column2 Value\"..\n";
echo "	GET,\"ID\"\n";
echo "	DELETE,ROW,\"ID\"\n"; 
echo "	EXIT to terminate the program\n"; 

//Read input from the user
$input=readline();	
$db=new Database();
$table=new Table(); 
//Checks the input of the user and calls a function accordingly
while ((strtoupper($input)) != "EXIT") {
	$query=explode(",",$input);
	//CREATE DATABASE
	if ($query[0] == "CREATE" && $query[1] == "DATABASE") {
		$databaseName=$query[2];
		$databaseName=str_replace('"', "", $databaseName);
		$db->createDatabase($databaseName);
	//DELETE DATABASE
	} else if ($query[0] == "DELETE" && $query[1] == "DATABASE") {
		$databaseName=$query[2];
		$databaseName=str_replace('"', "", $databaseName);
		$db->deleteDatabase($databaseName);
	//CREATE TABLE
	} else if ($query[0] == "CREATE" && $query[1] == "TABLE") {
		$columns=array();
		for($i=4;$i<count($query);$i++){
			$query[$i]=str_replace('"', "", $query[$i]);
			array_push($columns,$query[$i]);
		}
		$databaseName=$table->checkLatestFolder();
		$tableName=str_replace('"', "", $query[2]);
		$table->createTable($databaseName,$tableName,$columns);
	//DELETE TABLE
	} else if ($query[0] == "DELETE" && $query[1] == "TABLE") {
		$databaseName=$table->checkLatestFolder();
		$tableName=str_replace('"', "", $query[2]);
		$table->deleteTable($databaseName,$tableName);
	//ADD RECORD
	} else if ($query[0]=="ADD") {
		$databaseName=$table->checkLatestFolder();
		$tableName=$table->checkLatestFile();
		$file=fopen("$databaseName/$tableName","r");
		$line = fgetcsv($file);
		$record=array();
		for ($i = 1; $i < count($query); $i++) {
			$query[$i]=str_replace('"', "", $query[$i]);
			if ($query[$i] == "") {
				echo "Cannot have null values!\n";
			} else {
				array_push($record,$query[$i]);
			}
		}
		if (count($record) < count($line)) {
			echo "Cannot have null values!\n";
		} else {
			$table->addRecord($databaseName,$tableName,$record);
		}
	//GET RECORD
	} else if ($query[0] == "GET") {
		$databaseName=$table->checkLatestFolder();
		$tableName=$table->checkLatestFile();			
		$records=$table->getRecord($databaseName,$tableName,str_replace('"', "", $query[1]));
		if ($records != "This record does not exist!\n") {
			foreach ($records as $record) {			
				foreach ($record as $key => $value) {
					if ($key<count($record)-1) {
						echo "\"$value\"".", ";
					} else {
						echo "\"$value\"\n";
					}
				}
			}
		} else {
			echo $records;
		}
	//DELETE RECORD
	} else if ($query[0] == "DELETE" && $query[1] == "ROW") {
		$databaseName=$table->checkLatestFolder();
		$tableName=$table->checkLatestFile();			
		$table->deleteRecord($databaseName,$tableName,str_replace('"', "", $query[2]));
	}
		$input=readline();
}
exit();
?>