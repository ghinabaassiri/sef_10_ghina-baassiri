<?php
class Table 
{
	/**
	 * Creates a csv file where the database tables will be saved.
	 *
 	 * @param string $databaseName Input as string value of the database name.
 	 * @param string $tableName Input as string value of the table name.
 	 * @param array of strings $columns Input as an array of strings of the table columns.
 	 */
	public function createTable($databaseName,$tableName,$columns)
	{
		if (!file_exists ("$databaseName/$tableName".".csv")) {
			$file=fopen("$databaseName/$tableName".".csv","w");
			fputcsv($file,$columns);
			echo "\"$tableName\" CREATED\n";
			fclose($file);
		} else {
			echo "This table already exists!\n";
		}
	}
	/**
	 * Deletes a csv file where the table lies.
	 *
 	 * @param string $databaseName Input as string value of the database name.
 	 * @param string $tableName Input as string value of the table name.
 	 */
	public function deleteTable($databaseName,$tableName)
	{
		if (file_exists ("$databaseName/$tableName".".csv")) {
			unlink("$databaseName/$tableName".".csv");
			echo "\"$tableName\" DELETED\n";
		} else {
			echo "This table doesn't exist!\n";
		}	
	}
	/**
	 * Adds a record to the file/table.
	 *
 	 * @param string $databaseName Input as string value of the database name.
 	 * @param string $tableName Input as string value of the table name.
 	 * @param array of strings Input as array of string containing the records for each column.
 	 */
	public function addRecord($databaseName,$tableName,$record)
	{
		if ($this->searchRecord($databaseName,$tableName,$record[0])==false) {
			$file=fopen("$databaseName/$tableName","a") or die("can't open the file");
			fputcsv($file,$record);
			echo "Record ADDED\n";
			fclose($file);
		} else {
			echo "A record with the same id number already exists!\n";
		}
	}
	/**
	 * CheckLatestFile function.
	 *
 	 * @return string Output the name of the latest created file.
 	 */
	public function checkLatestFile()
	{
		/* List all files in the directory in descending order, then, whichever one is first in that list has the "greatest" 
		 * filename and therefore the greatest timestamp value then it is the newest.
		 */
		$directory=$this->checkLatestFolder();
		if (is_dir($directory)) {
			$files = scandir("$directory/", SCANDIR_SORT_DESCENDING);
			$latestFilename = $files[0];
			return $latestFilename;
		}
	}
	/**
	 * CheckLatestFolder function.
	 *
 	 * @return string Output the name of the latest created folder.
 	 */
	public function checkLatestFolder()
	{
		//get current directory
		$path = getcwd(); 
		$latestCreationTime = 0;
		$latestFilename = '';    

		$directory = dir($path);
		while (false !== ($entry = $directory->read())) {
 			$filepath = "{$path}/{$entry}";
			if (is_dir($filepath) && filectime($filepath) > $latestCreationTime && $entry!="." && $entry!="..") {
				$latestCreationTime = filectime($filepath);
				$latestFilename = $entry;
			}
		}
		return $latestFilename;
	}
	/**
	 * SearchRecord function.
	 *
	 * @param string $databaseName Input as string value of the database name.
 	 * @param string $tableName Input as string value of the table name.
 	 * @param string $id Input as string value upon which the records are searched for.
 	 * @return bool Output true if the record exists in the file.
 	 */
	public function searchRecord($databaseName,$tableName,$id)
	{
		$file=fopen("$databaseName/$tableName","r") or die("can't open the file");
		while (($line=fgetcsv($file)) !== false) {
			if ($id == $line[0]) {
				return true;
			}
		}
		return false;
	}
	/**
	 * GetRecord function.
	 *
	 * @param string $databaseName Input as string value of the database name.
 	 * @param string $tableName Input as string value of the table name.
 	 * @param string $id Input as string value upon which the records are searched for.
 	 * @return array of strings Output the record retrieved based on the given id.
 	 */
	public function getRecord($databaseName,$tableName,$id)
	{
		$file=fopen("$databaseName/$tableName","r") or die("can't open the file");
		$listOfRecords=array();
		while (($line=fgetcsv($file)) !== false) {
			if (in_array($id, $line)) {
				array_push($listOfRecords, $line);
   			}
		}
		if (!empty($listOfRecords)) {
			return $listOfRecords;
		} else {
			$message="This record does not exist!\n";
			return $message;
		}
	}
	/**
	 * DeleteRecord function.
	 *
	 * @param string $databaseName Input as string value of the database name.
 	 * @param string $tableName Input as string value of the table name.
 	 * @param string $id Input as string value upon which the records are searched for.
 	 */
	public function deleteRecord($databaseName,$tableName,$id)
	{
		$search=$this->searchRecord($databaseName,$tableName,$id);
		$lines=array();
		if ($search == true) {
			$read=fopen("$databaseName/$tableName", "r") or die("can't open the file");
			while (($line=fgetcsv($read)) !== false) {
				if (!in_array($id, $line)) {
					array_push($lines, $line);
				}
			}
			fclose($read);
			$write=fopen("$databaseName/$tableName", "w") or die("can't open the file");
			foreach ($lines as $line) {
			    fputcsv($write, $line);
			}
			fclose($write);
			echo "ROW DELETED\n";
		} else {
			echo "This record does not exist!\n";
		}
	}
}
?>[]