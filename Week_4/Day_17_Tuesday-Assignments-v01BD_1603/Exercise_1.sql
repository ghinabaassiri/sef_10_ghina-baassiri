--CREATE DATABASE `FinanceDB`;
USE `FinanceDB`;
--Dropping the table before recreating it again for testing purposes
DROP TABLE IF EXISTS `FiscalYearTable`;


CREATE TABLE `FiscalYearTable` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `start_date` DATE NOT NULL,
  `end_date` DATE NOT NULL,
  `fiscal_year` YEAR NOT NULL,
  UNIQUE KEY `fiscal_year_UNIQUE` (`fiscal_year`)
);
DELIMITER ;;
CREATE TRIGGER `check_date` BEFORE INSERT ON `FiscalYearTable` 
FOR EACH ROW
BEGIN 
IF NEW.start_date >= NEW.end_date THEN 
SIGNAL SQLSTATE '45000' 
SET MESSAGE_TEXT = 'start date is less than end date';
END IF;

IF (NEW.start_date  = '0000-00-00' OR New.end_date = '0000-00-00' ) THEN 
SIGNAL SQLSTATE '45000' 
SET MESSAGE_TEXT = 'Date format is not correct';
END IF;

IF (!(NEW.end_date REGEXP '^[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$') 
    || !(NEW.start_date REGEXP '^[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$'))
SIGNAL SQLSTATE '45000' 
SET MESSAGE_TEXT = 'Date format is not correct';
END IF;


IF(!NEW.fiscal_year regexp '^[0-9]{4}$')
SIGNAL SQLSTATE '45000' 
SET MESSAGE_TEXT = 'Year format is not correct';

END;
DELIMITER ;




